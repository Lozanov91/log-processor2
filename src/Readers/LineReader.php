<?php

namespace LogProcessor\Readers;

use Exception;

class LineReader
{
    private $line;
    private $pointer;

    const DATE_POS   = 0;
    const TIME_POS   = 1;
    const STATUS_POS = 7;
    const BYTES_POS  = 8;
    const HOST_POS   = 10;
    const LINE_ELEMENTS = 18;

    /**
     * LineReader constructor.
     * @param array $line
     * @param int $pointer
     */
    public function __construct(array $line, int $pointer)
    {
        $this->line    = $line;
        $this->pointer = $pointer;
    }

    /**
     * @return array
     */
    public function getLine() : array
    {
        return $this->line;
    }

    /**
     * @return array
     * @throws Exception
     */
    public function processLine() : array
    {
        $result = [];

        try {
            $result['data'] = $this->getLineData();
        } catch (\RuntimeException $exception) {
            $result['data']['error'] = $exception->getMessage();
        }

        return $result;
    }

    /**
     * This function will try to extract data from the line string
     * based on data positions. Defined in constants.
     *
     * @return array
     * @throws Exception
     */
    private function getLineData() : array
    {
        $data = [];
        $line = $this->line;

        $this->checkLineElementsMatch(count($line));

        $data['host']  = $this->getHost($line);
        $data['date']  = $this->getDate($line);
        $data['time']  = $this->getTime($line);
        $rawStatus     = $this->getStatus($line[self::STATUS_POS]);
        $data['tcp']   = $this->getTcp($line, $rawStatus);
        $data['code']  = $this->getCode($line, $rawStatus);
        $data['bytes'] = $this->getBytes($line);

        return $data;
    }

    /**
     * @param int $lineCount
     */
    private function checkLineElementsMatch(int $lineCount) : void
    {
        if (self::LINE_ELEMENTS != $lineCount) {
            throw  new \RuntimeException("Expecting ".self::LINE_ELEMENTS." fields but received {$lineCount} on line {$this->pointer}");
        }
    }

    /**
     * @param $line
     * @return int
     */
    private function getBytes(&$line) : int
    {
        if (!ctype_digit($line[self::BYTES_POS])) {
            throw new \RuntimeException("Invalid Bytes Number in :{$line[self::BYTES_POS]} on line {$this->pointer}");
        }

        return $line[self::BYTES_POS];
    }

    /**
     * @param $line
     * @param $rawStatus
     * @return string
     */
    private function getTcp(&$line, $rawStatus) : string
    {
        if (!$rawStatus) {
            throw new \RuntimeException("Invalid Cache Status in :{$line[self::STATUS_POS]} on line {$this->pointer}");
        }

        return $rawStatus['tcp'];
    }

    /**
     * @param $line
     * @param $rawStatus
     * @return string
     */
    private function getCode(&$line, $rawStatus) : string
    {
        if (!$rawStatus) {
            throw new \RuntimeException("Invalid Cache Status in :{$line[self::STATUS_POS]} on line {$this->pointer}");
        }

        return $rawStatus['code'];
    }

    /**
     * @param $line
     * @return string
     */
    private function getHost(&$line) : string
    {
        $url = parse_url($line[self::HOST_POS]);

        if (!isset($url['host'])) {
            throw new \RuntimeException("Invalid Host Name in :{$line[self::HOST_POS]} on line {$this->pointer}");
        }

        return $url['host'];
    }

    /**
     * @param $line
     * @return string
     */
    private function getDate(&$line) : string
    {
        if (!strtotime($line[self::DATE_POS])) {
            throw new \RuntimeException("Invalid date received :{$line[self::DATE_POS]} on line {$this->pointer}");
        }

        return $line[self::DATE_POS];
    }

    /**
     * @param $line
     * @return string
     * @throws Exception
     */
    private function getTime(&$line) : string
    {
        if (!strtotime($line[self::TIME_POS])) {
            throw new \RuntimeException("Invalid time received :{$line[self::TIME_POS]} on line {$this->pointer}");
        }

        return $this->roundDownToMinuteInterval(new \DateTime($line[self::TIME_POS]));
    }

    /**
     * @param string $status
     * @return array|bool
     */
    private function getStatus(string $status)
    {
        $status = explode('/', $status);

        if (count($status) != 2 || !ctype_digit($status[1])) {
            return false;
        }

        $isHit  = strpos(strtolower($status[0]), 'hit') !== false;
        $isMiss = strpos(strtolower($status[0]), 'miss') !== false;

        if (!$isHit && !$isMiss) {
            return false;
        }

        return ['tcp' => $isHit ? 'hit' : 'miss' , 'code' => $status[1]];
    }

    /**
     * @param \DateTime $dateTime
     * @param int $minuteInterval
     * @return string
     */
    private function roundDownToMinuteInterval(\DateTime $dateTime, $minuteInterval = 10) : string
    {
        return $dateTime->setTime(
            $dateTime->format('H'),
            floor($dateTime->format('i') / $minuteInterval) * $minuteInterval,
            0
        )->format('H:i');
    }
}