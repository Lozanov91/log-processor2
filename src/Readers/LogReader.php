<?php

namespace LogProcessor\Readers;

use Generator;

class LogReader
{
    private $readFile;
    private $sourceFile;

    /**
     * The log reader will iterate through each line of the file
     * and will perform methods to handle its data.
     *
     * LogReader constructor.
     * @param string $sourceFile
     */
    public function __construct(string $sourceFile)
    {
        $this->sourceFile = $sourceFile;
        $this->readFile = fopen($this->sourceFile, 'r');
    }

    /**
     * @return Generator
     */
    public function readLines()
    {
        while (!feof($this->readFile)) {
            yield fgets($this->readFile);
        }
    }

    /**
     * Class destructor
     * Close read file
     */
    public function __destruct()
    {
        if (!is_null($this->readFile)) {
            fclose($this->readFile);
        }
    }
}