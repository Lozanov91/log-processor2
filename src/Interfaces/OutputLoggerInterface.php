<?php

namespace LogProcessor\Interfaces;

interface OutputLoggerInterface
{
    /**
     * @param $output
     * @return mixed
     */
    public function output(array $output);
}