<?php

namespace LogProcessor;

use Exception;
use LogProcessor\Readers\LineReader;
use LogProcessor\Readers\LogReader;
use LogProcessor\Loggers\ErrorLogger;
use LogProcessor\Loggers\OutputLoggerJSON;

class LogProcessor
{
    private $logReader;
    private $errorLogger;
    private $outPutter;

    const HEADER_EL_FIRST = '#Fields:';

    /**
     * LogProcessor constructor.
     *
     * @param LogReader $logReader
     * @param ErrorLogger $errorLogger
     * @param OutputLoggerJSON $outPutter
     */
    public function __construct(LogReader $logReader, ErrorLogger $errorLogger, OutputLoggerJSON $outPutter)
    {
        $this->logReader   = $logReader;
        $this->errorLogger = $errorLogger;
        $this->outPutter   = $outPutter;
    }

    /**
     * Start processing the log file line by line
     *
     * @throws Exception
     */
    public function processLog() : void
    {
        $output = [];

        $pointer = 0;
        foreach ($this->logReader->readLines() as $stringLine) {
            $pointer++;

            $lineReader = new LineReader(str_getcsv($stringLine, ' '), $pointer);

            $line = $lineReader->getLine();

            if ($this->isEmptyLine($line)) {
                continue;
            }

            // validate if the first line is valid header and skip it
            if ($pointer == 1) {
                $this->validateHeader($line);
                continue;
            }

            $result = $lineReader->processLine();
            $row    = $result['data'];

            if (isset($row['error']) && $row['error'] !== false) {
                $this->errorLogger->logError($row['error']);
                continue;
            }

            if (!isset($output[$row['host']][$row['date']][$row['time']][$row['code']][$row['tcp']])) {
                $output[$row['host']][$row['date']][$row['time']][$row['code']][$row['tcp']] = ['requests' => 1, 'bytes' => intval($row['bytes'])];
            } else {
                $output[$row['host']][$row['date']][$row['time']][$row['code']][$row['tcp']]['requests']++;
                $output[$row['host']][$row['date']][$row['time']][$row['code']][$row['tcp']]['bytes'] += intval($row['bytes']);
            }
        }

        $this->outPutter->output($output);
    }

    /**
     * @param array $line
     * @return bool
     */
    private function isEmptyLine(array $line) : bool
    {
        return !isset($line[0]) || is_null($line[0]);
    }

    /**
     * @param array $line
     */
    private function validateHeader(array $line) : void
    {
        if ($line[0] !== self::HEADER_EL_FIRST) {
            throw new \InvalidArgumentException('Error: Log file does not contain valid header.');
        }
    }
}
