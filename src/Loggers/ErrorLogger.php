<?php

namespace LogProcessor\Loggers;

class ErrorLogger
{
    private $errorLogFile;

    /**
     * ErrorLogger constructor.
     * @param string $sourceFile
     */
    public function __construct(string $sourceFile)
    {
        $this->errorLogFile = pathinfo($sourceFile, PATHINFO_BASENAME).'.err';

        @unlink($this->errorLogFile);
        touch($this->errorLogFile);
    }

    /**
     * Write error to file
     *
     * @param string $error
     */
    public function logError(string $error) : void
    {
        if (file_put_contents($this->errorLogFile, $error . PHP_EOL, FILE_APPEND) === false) {
            throw new \RuntimeException('Cant log error to '. $this->errorLogFile);
        }
    }
}