<?php

namespace LogProcessor\Loggers;

use LogProcessor\Interfaces\OutputLoggerInterface;

class OutputLoggerJSON implements OutputLoggerInterface
{
    private $outputFile;

    /**
     * OutputLoggerJSON constructor.
     * @param string $sourceFile
     */
    public function __construct(string $sourceFile)
    {
        $this->outputFile = pathinfo($sourceFile, PATHINFO_FILENAME).'.json';
    }

    /**
     * Write the output into a file
     *
     * @param $output
     */
    public function output(array $output) : void
    {
        if (file_put_contents($this->outputFile, json_encode($output)) === false) {
            throw new \RuntimeException('Cant log output to '. $this->outputFile);
        }
    }
}