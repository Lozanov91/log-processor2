<?php
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';

use LogProcessor\LogProcessor;
use LogProcessor\Readers\LogReader;
use LogProcessor\Loggers\ErrorLogger;
use LogProcessor\Loggers\OutputLoggerJSON;

if (count($argv) != 2) {
    echo 'You missed to specify filepath. Run again.', PHP_EOL;
    exit;
}
$file = $argv[1];

if (!file_exists($file) || !is_file($file)) {
    echo 'File does not exist.', PHP_EOL;
    exit;
}

$logReader   = new LogReader($file);
$errorLogger = new ErrorLogger($file);
$outPutter   = new OutputLoggerJSON($file);

$lp = new LogProcessor($logReader, $errorLogger, $outPutter);

try {
    $lp->processLog();
} catch (\Throwable $exception) {
    echo $exception->getMessage(), PHP_EOL;
    exit;
}

echo 'Done.', PHP_EOL;