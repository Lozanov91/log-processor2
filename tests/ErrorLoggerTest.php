<?php

use LogProcessor\Loggers\ErrorLogger;

use PHPUnit\Framework\TestCase;

class ErrorLoggerTest extends TestCase
{
    private $sourceFile;
    private $errorFile;

    public function setUp() : void
    {
        $this->sourceFile = 'test-err-file.log';
        $this->errorFile  = $this->sourceFile.'.err';
    }

    public function testIsCreatedErrorFile()
    {
        $expected    = $this->errorFile;
        $errorLogger = new ErrorLogger($this->sourceFile);

        $this->assertFileExists($expected);
        $this->assertFileIsWritable($expected);
    }

    public function testCanLogToErrorFile()
    {
        $errorLogger = new ErrorLogger($this->sourceFile);

        $errorLogger->logError('some error');

        $this->assertEquals("some error\n", file_get_contents($this->errorFile));
    }

    public function testLogErrorExceptionMessage()
    {
        $this->markTestSkipped(
            'not done test : Can\'t catch right exception'
        );

//        $stub = $this->getMockBuilder('ErrorLogger')
//            ->setMethods(array(
//                'logError'
//            ))
//            ->disableAutoload()
//            ->disableOriginalConstructor()
//            ->getMock();
//
//
//        $stub->expects($this->any())
//            ->method('logError')
//            ->will($this->returnValue(RuntimeException::class));
//
//        $this->expectException(RuntimeException::class);
//        $stub->logError('test-error');

//
//        $this->expectExceptionMessage('Cant log error to _-_/\$$brokenFile00_!).err');
//        $this->expectException(RuntimeException::class);
//        $logger = new ErrorLogger('_-_/\$$brokenFile00_!)');
//        $logger->logError('__123/321fqwfq**error');
    }

    public function tearDown(): void
    {
        @unlink($this->errorFile);
    }
}