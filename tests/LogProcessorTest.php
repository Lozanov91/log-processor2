<?php

use LogProcessor\Loggers\ErrorLogger;
use LogProcessor\Loggers\OutputLoggerJSON;
use LogProcessor\Readers\LineReader;
use LogProcessor\Readers\LogReader;
use LogProcessor\LogProcessor;

use PHPUnit\Framework\TestCase;

class LogProcessorTest extends TestCase
{
    private $logReader;
    private $sourceFile;

    public function setUp() : void
    {
        $this->sourceFile = __DIR__.DIRECTORY_SEPARATOR.'files'.DIRECTORY_SEPARATOR.'test-log.log';
        $this->logReader  = new LogReader($this->sourceFile);
    }

    public function testLogProcessorProcessFile()
    {
        $logReader = $this
            ->getMockBuilder(LogReader::class)
            ->disableOriginalConstructor()
            ->setMethods(["readLines"])
            ->getMock();

        $logReader->expects($this->any())
            ->method("readLines")
            ->will($this->returnCallback(function () {
                $data = [
                    1 => '#Fields: date time time-taken c-ip filesize s-ip s-port sc-status sc-bytes cs-method cs-uri-stem - rs-duration rs-bytes c-referrer c-user-agent customer-id x-ec_custom-1',
                    2 => '2014-04-02 02:12:09 0 113.67.181.139 2863 68.232.40.109 80 TCP_HIT/200 3125 GET http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80 - 0 1214 "-" "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36" 26792 "http://results.chemistdirect.co.uk/search?p=Q&lbc=chemistdirect-uk&uid=18973090&ts=redesign&w=Baby&isort=score&method=and&view=grid&af=department:babychildcare&format=cat1"',
                    3 => '2014-04-02 02:00:16 1 159.253.135.146 55726 46.22.73.243 80 TCP_HIT/200 55989 GET http://asset3.turfcdn.com/bnx@DLT2Lasz8Sg@csGRqwwTC9wj.jpg?w=630&h=630&q=80 - 0 381 "-" "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:14.0) Gecko/20100101 Firefox/14.0.1" 26792 "-"',
                ];
                foreach ($data as $k => $e) {
                    // return a generator
                    yield new LineReader(str_getcsv($e, ' '), $k);
                }
            }));

        $errorLogger  = new ErrorLogger($this->sourceFile);
        $outPutter    = new OutputLoggerJSON($this->sourceFile);
        $logProcessor = new LogProcessor($logReader, $errorLogger, $outPutter);

        $logProcessor->processLog();
        $this->assertFileExists('test-log.json');
    }
}