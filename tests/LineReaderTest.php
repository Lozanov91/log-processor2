<?php

use LogProcessor\Readers\LineReader;

use PHPUnit\Framework\TestCase;

class LineReaderTest extends TestCase
{
    private $lineReader;
    private $lineData;

    public function setUp() : void
    {
        $this->lineData = [
            0 => "2014-04-02",
            1 => "02:12:09",
            2 => "0",
            3 => "113.67.181.139",
            4 => "2863",
            5 => "68.232.40.109",
            6 => "80",
            7 => "TCP_HIT/200",
            8 => "3125",
            9 => "GET",
            10 => "http://images.chem.co.uk/SMA-Wysoy-Soya-Infant-Formula-192594.jpg?o=gL1biz9dm5vMSjIVW9Npa@nLNCIj&V=iIxj&h=97&w=97&q=80",
            11 => "-",
            12 => "0",
            13 => "1214",
            14 => "-",
            15 => "Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36",
            16 => "26792",
            17 => "http://results.chemistdirect.co.uk/search?p=Q&lbc=chemistdirect-uk&uid=18973090&ts=redesign&w=Baby&isort=score&method=and&view=grid&af=department:babychildcare&format=cat1"
        ];

        $this->lineReader = new LineReader($this->lineData, 2);
    }

    public function testGetRowIsValid()
    {
        $actual = $this->lineReader->processLine();
        $expected = [
            "data" => [
                "host" => "images.chem.co.uk",
                "date" => "2014-04-02",
                "time" => "02:10",
                "tcp" => "hit",
                "code" => "200",
                "bytes" => 3125,
            ]
        ];

        $this->assertArrayHasKey('data', $actual);

        $this->assertContains('images.chem.co.uk', $actual['data']);
        $this->assertContains('2014-04-02', $actual['data']);
        $this->assertContains('02:10', $actual['data']);
        $this->assertContains('hit', $actual['data']);
        $this->assertContains('200', $actual['data']);
        $this->assertContains('3125', $actual['data']);
        $this->assertEquals($expected, $actual);
    }

    public function testItFailsIfElementsCountDontMatchTheHeader()
    {
        $lineData = $this->lineData;
        $lineData['one_more'] = 'some more';

        $lineReader = new LineReader($lineData, 2);
        $result = $lineReader->processLine();
        $this->assertArrayHasKey('error', $result['data']);
    }

    public function testItFailsIfRowHasInvalidBytes()
    {
        $data = $this->lineData;
        $data[8] = 'invalidBytes';

        $lineReader = new LineReader($data, 2);
        $result = $lineReader->processLine();

        $this->assertArrayHasKey('error', $result['data']);
        $this->assertEquals('Invalid Bytes Number in :invalidBytes on line 2', $result['data']['error']);
    }

    public function testItFailsIfRowHasInvalidTcpOrCode()
    {
        $data = $this->lineData;
        $data[7] = 'NotATcp';

        $lineReader = new LineReader($data, 2);
        $result = $lineReader->processLine();

        $this->assertArrayHasKey('error', $result['data']);
        $this->assertEquals('Invalid Cache Status in :NotATcp on line 2', $result['data']['error']);
    }

    public function testItFailsIfRowHasInvalidHost()
    {
        $data = $this->lineData;
        $data[10] = 'not-valid-url';

        $lineReader = new LineReader($data, 2);
        $result = $lineReader->processLine();

        $this->assertArrayHasKey('error', $result['data']);
        $this->assertEquals('Invalid Host Name in :not-valid-url on line 2', $result['data']['error']);
    }

    public function testItFailsIfRowHasInvalidDate()
    {
        $data = $this->lineData;
        $data[0] = 'not-valid-date';

        $lineReader = new LineReader($data, 2);
        $result = $lineReader->processLine();

        $this->assertArrayHasKey('error', $result['data']);
        $this->assertEquals('Invalid date received :not-valid-date on line 2', $result['data']['error']);
    }

    public function testItFailsIfRowHasInvalidTime()
    {
        $data = $this->lineData;
        $data[1] = 'not-valid-time';

        $lineReader = new LineReader($data, 2);
        $result = $lineReader->processLine();

        $this->assertArrayHasKey('error', $result['data']);
        $this->assertEquals('Invalid time received :not-valid-time on line 2', $result['data']['error']);
    }
}
