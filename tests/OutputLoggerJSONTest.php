<?php

use LogProcessor\Loggers\OutputLoggerJSON;

use PHPUnit\Framework\TestCase;

class OutputLoggerJSONTest extends TestCase
{
    private $outPutter;
    private $outputFile;

    public function setUp() : void
    {
        $this->outputFile = 'test-output-file.json';
        $this->outPutter = new OutputLoggerJSON($this->outputFile);
    }

    public function testCanLogOutput()
    {
        $this->outPutter->output([]);
        $this->assertFileExists($this->outputFile);
        $this->assertEquals('[]', file_get_contents($this->outputFile));
    }

    public function testCanLogJsonOutput()
    {
        $this->outPutter->output(['foo'=>'bar']);
        $this->assertFileExists($this->outputFile);
        $this->assertEquals('{"foo":"bar"}', file_get_contents($this->outputFile));
    }

    public function tearDown(): void
    {
        @unlink($this->outputFile);
    }
}